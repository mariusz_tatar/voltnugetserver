# VoltNugetServer

Serwer nuget-a do wewn�trznych bibliotek 96Volt

## Instalacja 

Aby mo�na by�o korzysta� z nuget-a nale�y:

* Wykona� publish tej aplikacji i umie�ci� j� w IIS. 
* Skonfigurowa� web.config, a w szczeg�lno�ci miejsce gdzie b�d� przetrzymywane pakiety oraz klucz (has�o)
* Uruchomi� aplikacj� na IIS
* W visual studio w ustawieniach Nuget doda� now� lokalizacj� (adres kt�ry podali�my podczas instalacji na IIS)
* Zrestartowa� VS

Od tej pory powinny by� r�nie� wy�wietlana pakiety z naszej nowej lokalizacji (w prawym roku ekranu w managerze nuget nal;e�y jeszcze przestawi� �r�d�o na  All)
Wszystkie komendy nuget-a dzia�aja na naszym serwerze.

Aktualnie na erwerze 96 volt jest pod adresem: http://nugetserver.96volt.com/nuget

